import React from 'react';
import reactDom from 'react-dom';
import ReactDom from 'react-dom';
import Article from './Article'
import PublisherDetail from './PublisherDetail'

const articleObj = {
    title : 'Inside Anthony Hopkins’s Unexpected Win at the Oscars',
    discription : 'The actor offered to accept via Zoom but the academy said no. Still, he won because of a release strategy that ensured his film would peak at the last minute.',
    imageSrc : 'https://static01.nyt.com/images/2021/04/26/arts/26projectionist/merlin_183994146_02c1f1d6-5fc1-4c7f-a5d0-d2ded5b8b877-jumbo.jpg?quality=90&auto=webp'

}

const publishObj = {
    author : 'Kyle Buchanan',
    date : 'April 26, 2021'
}
 

const App = () => {
    return (
        <div class="ui container" style = {{marginTop : '50px'}}>
            <Article title = {articleObj.title} discription = {articleObj.discription} image = {articleObj.imageSrc} >
                <PublisherDetail author = {publishObj.author} date = {publishObj.date} />
            </Article>

        </div>
    )
};

reactDom.render(<App /> , document.querySelector('#root'));