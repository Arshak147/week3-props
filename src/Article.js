import react from 'react';


const Article = (props) => {
   return (
    <div className="ui centered card">
    <div className="image">
      <img src={props.image}></img>
    </div>
    <div className="content">
      <a className="header">{props.title}</a>
      <div className="meta">
        <span className="date">{props.children}</span>
      </div>
      <div className="description">
        {props.discription}
      </div>
    </div>
   
  </div>
        );
      
}

export default Article ; 