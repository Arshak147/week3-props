import React from 'react';

const PublisherDetail = (props) => {
    return (
        <div>
            <div> published by :{props.author} </div>
            <div> Date : {props.date}</div>
        </div>
    )
};

export default PublisherDetail;